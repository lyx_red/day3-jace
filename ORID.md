# ORID



## Objective

- First, I mainly conducted Code Review in the team, which was the first time for me to conduct Code Review with my team members. I think by checking each other's code, we can learn each other's advantages, and at the same time, we can let others help us find our shortcomings, which is very conducive to improving our coding ability.
- In addition, today we started to learn the Stream API in Java and did a lot of stream-related exercises.
- Learn the basic knowledge of OOP, and have a deeper understanding of java object-oriented features. Concepts such as encapsulation, inheritance, polymorphism, etc. are more deeply understood. And do some exercises about OOP.



## Reflective

I find it very fulfilling and challenging

## Interpretative

Because I learned some new Strem apis about java, I think it is very helpful for my work and study. The reason I find it challenging is that it is difficult to really apply OOP ideas to more responsible objects.

## Decision

In the future Coding, I will use Stream API more. At the same time, I will ask myself to take OOP ideas into consideration in coding, so that I can skillfully use Stream API and learn OOP ideas.

 