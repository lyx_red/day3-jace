package ooss;

public class Student extends Person {
    private Klass klass;

    public Student(int id, String name, int age) {
        super(id, name, age);
    }

    public Klass getKlass() {
        return klass;
    }

    @Override
    public String introduce() {
        String studentIntroduce = String.format("My name is %s. I am %d years old. I am a student.", getName(), getAge());
        if (isIn(klass) && klass.isLeader(this)) {
            return studentIntroduce + String.format(" I am the leader of class %d.", klass.getNumber());
        } else if (isIn(klass)) {
            return studentIntroduce + String.format(" I am in class %d.", klass.getNumber());
        } else {
            return studentIntroduce;
        }
    }

    public void join(Klass klass) {
        this.klass = klass;
    }

    public boolean isIn(Klass klass) {
        if (klass == null) return false;
        return this.klass == klass;
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    public void update(Student leader) {
        System.out.println(String.format("I am %s, student of Class %d. I know %s become Leader.", getName(), leader.getKlass().getNumber(), leader.getName()));
    }
}
