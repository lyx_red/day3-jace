package ooss;

import java.util.ArrayList;
import java.util.List;

public class Klass {
    private int number;

    private Student leader;

    // 观察者列表
    private List<Person> personList = new ArrayList<>();

    public Klass(int number) {
        this.number = number;
    }

    public Student getLeader() {
        return leader;
    }

    public int getNumber() {
        return number;
    }

    @Override
    public boolean equals(Object obj) {
        Klass kclass = (Klass) obj;
        return kclass.number == number;
    }

    public void assignLeader(Student student) {
        boolean isInClass = student.isIn(this);
        if (isInClass) {
            this.leader = student;
            notifyPersons(student);
        } else {
            System.out.println("It is not one of us.");
        }
    }

    public boolean isLeader(Student student) {
        return leader == student;
    }

    public void attach(Person person) {
        // 将指定的Person对象添加到观察者列表
        this.personList.add(person);
    }
    public void notifyPersons(Student leader) {
        personList.forEach(person -> person.update(leader));
    }
}
