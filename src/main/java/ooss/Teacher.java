package ooss;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.StringJoiner;
import java.util.stream.Collectors;

public class Teacher extends Person {

    private List<Klass> classList = new ArrayList<>();

    public Teacher(int id, String name, int age) {
        super(id, name, age);
    }

    @Override
    public String introduce() {
        String teacherIntroduce = String.format("My name is %s. I am %d years old. I am a teacher.", getName(), getAge());
        if (classList.size() > 0) {
            StringJoiner stringJoiner = new StringJoiner(", ");
            String teachClass = " I teach Class ";
            classList.stream().map(klass -> String.valueOf(klass.getNumber())).forEach(stringJoiner::add);
            teachClass += stringJoiner.toString();
            return teacherIntroduce + teachClass + ".";
        }
        return teacherIntroduce;
    }

    public void assignTo(Klass klass) {
        if (klass != null) {
            this.classList.add(klass);
        }
    }

    public boolean belongsTo(Klass klass) {
        return classList.contains(klass);
    }

    public boolean isTeaching(Student student) {
        return classList.contains(student.getKlass());
    }

    @Override
    public void update(Student leader) {
        System.out.println(String.format("I am %s, teacher of Class %d. I know %s become Leader.", getName(), leader.getKlass().getNumber(), leader.getName()));
    }
}
